<html>
<head>
	<title>Login Page - Index</title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
	<script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
	
	<!--this is for the footer-->
	<link rel="stylesheet" href="css/footer.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
	
	<!--end footer-->

	<!-- Header Navigation -->
	<link rel="stylesheet" href="css/headNav.css">

	<style type="text/css">
		#loginform{
			padding-top: 10%;
		}
	</style>

</head>

<body>

<div class="mdl-layout__header" id="header" align="center">
	<h2>Welcome to QuizzedIn</h2>

</div>
	
  <div class="mdl-layout__header" style="background-color:#333;" id="header" align="center">
		<div style="padding-left:50px;padding-right:50px">
			<ul class="ul1">
			  <li class="li1"><a href="#home">Home</a></li>
			  <li class="li1"><a href="#news">News</a></li>
			  <li class="li1"><a href="#contact">Contact</a></li>
			  <li class="li1" style="float:right"><a href="#about">About</a></li>
			</ul>
		</div>
  </div>


				<div align="center" id="loginform">
					<form action="Login.php" method="post" autocomplete="off">

						<div class="mdl-textfield mdl-js-textfield">
							<i class="material-icons">account_circle</i>
			    			<input class="mdl-textfield__input" type="text" id="sample1" name="username">
			    			<label class="mdl-textfield__label" for="sample1">Username</label>
			  			</div>
			  			<br>
			  			<div class="mdl-textfield mdl-js-textfield">
			  				<i class="material-icons">lock</i>
			    			<input class="mdl-textfield__input" type="password" id="sample1" name="password">
			    			<label class="mdl-textfield__label" for="sample1">Password</label>
			  			</div>
						<br>
						<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Login <i class="fa fa-sign-in" aria-hidden="true"></i></button>
						<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" formaction="signup.php">Sign Up</button>

						
						<!-- <input type="submit" name="Submit" value="Log In"> -->

					
					
					</form>

				</div>

<!--footer-->	
		<footer class="footer-distributed">
			<div class="footer-left">
				<h3>Company<span>logo</span></h3>
				<p class="footer-links">
					<a href="#">Home</a>
					·
					<a href="#">Blog</a>
					·
					<a href="#">Pricing</a>
					·
					<a href="#">About</a>
					·
					<a href="#">Faq</a>
					·
					<a href="#">Contact</a>
				</p>
				<p class="footer-company-name">Company Name &copy; 2015</p>
			</div>
			<div class="footer-center">
				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>21 Revolution Street</span> Paris, France</p>
				</div>
				<div>
					<i class="fa fa-phone"></i>
					<p>+1 555 123456</p>
				</div>
				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">support@company.com</a></p>
				</div>
			</div>
			<div class="footer-right">
				<p class="footer-company-about">
					<span>About the company</span>
					Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
				</p>
				<div class="footer-icons">
					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>
				</div>
			</div>
		</footer>

</body>
</html>