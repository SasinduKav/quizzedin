<html>
<head>
	<title>Login Page - Index</title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
	<script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
	
	<!--this is for the footer-->
	<link rel="stylesheet" href="css/footer.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
	
	<!--end footer-->

	<!-- Header Navigation -->
	<link rel="stylesheet" href="css/headNav.css">

	<!-- Question View -->
	<link rel="stylesheet" href="css/qview.css">
	
	<style type="text/css">
		#loginform{
			padding-top: 5%;
		}
	</style>

	<style>
		ul {list-style-type: none;}


		.qBox {
		    margin: 0;
		    padding: 5px 0;
		    background-color: #ddd;
		}

		.qBox li {
		    display: inline-block;
		    width: 90%;
		    color: #666;
		    text-align: center;
		}

		.ques {
		    padding: 10px 0;
		    background: #eee;
		    margin: 0;
		}

		.ques li {
		    list-style-type: none;
		    display: inline-block;
		    width: 20%;
		    text-align: center;
		    margin-bottom: 5px;
		    font-size:12px;
		    color: #777;
		}

		.ques li .active {
		    padding: 5px;
		    background: #1abc9c;
		    color: white !important
		}

		.ques li .cmp {
		    padding: 4px;
		    background: #999966;
		    color: white !important
		}

		a { text-decoration: none; }

		</style>



<?php
$connect=mysqli_connect("localhost","user","root") or die("Couldn't connect to MySql");
mysqli_select_db($connect,"wad") or die("couldn't connect to database");
?>





</head>

<body>

	<!-- Taking the question number by the previous page -->
	<?php
		$quizNo=$_GET['quiz'];
		$ques=$_GET['var'];
	?>



<div class="mdl-layout__header" id="header" align="center">
	<h2>Welcome to QuizzedIn</h2>

</div>
	
  <div class="mdl-layout__header" style="background-color:#333;" id="header" align="center">
		<div style="padding-left:50px;padding-right:50px">
			<ul class="ul1">
			  <li class="li1"><a href="#home">Home</a></li>
			  <li class="li1"><a href="#news">News</a></li>
			  <li class="li1"><a href="#contact">Contact</a></li>
			  <li class="li1" style="float:right"><a href="#about">About</a></li>
			</ul>
		</div>
  </div>

				<div style="padding-left:60px;width:22.9%;height:600px;float:left;background-color:white !important" id="loginform">
					<form action="" method="post" autocomplete="off">
						<div class="mdl-textfield mdl-js-textfield" style="padding-right:50px">
							<ul class="qBox">
							  <li>Question List</li>
							</ul>

							<ul class="ques">

							<?php

								for($i=1;$i<21;$i++)
								{
									if($i==$ques)
									{
							?>
									<li>
										<?php
										echo '<a href="qView.php?quiz='.$quizNo.' & var='.$i.' ">'; 
										echo '<span class="active"> '.$i.'</span>';
										echo '</a>';
										?>
										</li>

							<?php
									}
									else
									{
											$qu=mysqli_query($connect,"SELECT markedAns from student_marks WHERE quizid='$quizNo' and qNo='$i'");
											while ($rw=mysqli_fetch_assoc($qu)) {
													$val=$rw['markedAns'];
												if($val==null){
													$val="N";
												}
											}


										if ($val!="N") 
										{
											echo '<li>';
											echo '<a href="qView.php?quiz='.$quizNo.' & var='.$i.' ">'; 
											echo '<span class="cmp"> '.$i.'</span>';
											echo '</a>';
											echo '</li>';
										}
										else
										{
							?>
											<li>
											<?php
												echo '<a href="qView.php?quiz='.$quizNo.' & var='.$i.' ">'; 
												echo ' '.$i.'';
												echo '</a>';
											?>
											</li>
							<?php			
										}
									}
								}	
							?>
							</ul>

							<ul class="qBox">

								<li>
								  	<ul class="ques">
							  			<li style="width:100% !important"><span class="active"> &nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;Current Question</li>
							  		</ul>
							  	</li>
							  	
							  	<li>
							  		<ul class="ques">
							  			<li style="width:100% !important"><span class="cmp"> &nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;Completed Questions</li>
							  		</ul>
							  	</li>
							</ul>



			  			</div>				
					</form>
				</div>









				<div style="padding-left:100px;width:65%;height:600px;float:right;background-color:white !important" id="loginform">
					<form action="qMarkUpdate.php" method="post" autocomplete="off">
							<?php
							
								$query=mysqli_query($connect,"SELECT question from quiz WHERE qNo='$quizNo' and questionNo='$ques'");
								$numrows=mysqli_num_rows($query);
								while ($row=mysqli_fetch_assoc($query)) {
									$qname=$row['question'];
								}

							?>							
							<h3><?php echo $qname ?></h3>
							<div>
							  <ul class="ulll">
							  	<?php
							
									$query2=mysqli_query($connect,"SELECT answer,answer1,answer2,answer3,answer4 from quiz WHERE qNo='$quizNo' and questionNo='$ques'");
									$numrows2=mysqli_num_rows($query2);
									while ($row2=mysqli_fetch_assoc($query2)) {
										$ans=$row2['answer'];
										$ans1=$row2['answer1'];
										$ans2=$row2['answer2'];
										$ans3=$row2['answer3'];
										$ans4=$row2['answer4'];
										$ansArray = array($ans1,$ans2, $ans3,$ans4);
									}
//Update with the Student session and query with student in where clause
									$query3=mysqli_query($connect,"SELECT markedAns from student_marks WHERE quizid='$quizNo' and qNo='$ques'");
									$numrows3=mysqli_num_rows($query3);
									while ($row3=mysqli_fetch_assoc($query3)) {
											$Mans=$row3['markedAns'];
										if($Mans==null){
											$Mans="N";
										}
										
									}

									if($Mans!="N"){
										for($a=1;$a<5;$a++){
											if($Mans==$a){
												echo' <li>';
												echo'   <input type="radio" id="'.$a.'"  value="'.$a.'" name="selector" checked>';
												echo'    <label for="'.$a.'">'.$ansArray[$a-1].'</label><div class="check"></div>';
												echo' </li>';
											}
											else{
												echo' <li>';
												echo'   <input type="radio" id="'.$a.'" value="'.$a.'" name="selector">';
												echo'    <label for="'.$a.'">'.$ansArray[$a-1].'</label><div class="check"></div>';
												echo' </li>';
											}
										}
									}
									else{

								?>	
										 <li>
										    <input type="radio" id="1" value="1" name="selector">
										    <label for="1"><?php echo $ans1 ?></label><div class="check"></div>
										 </li>
										 <li>
										    <input type="radio" id="2" value="2" name="selector">
										    <label for="2"><?php echo $ans2 ?></label><div class="check"></div>
										 </li>
										 <li>
										    <input type="radio" id="3" value="3" name="selector">
										    <label for="3"><?php echo $ans3 ?></label><div class="check"></div>
										 </li>
										 <li>
										    <input type="radio" id="4" value="4" name="selector">
										    <label for="4"><?php echo $ans4 ?></label><div class="check"></div>
										 </li>
								<?php
									}
								?>	
							  </ul>
							</div>
							<input type="submit" value="submit" name="submit">
					</form>
				</div>






<!--footer-->	
		<footer class="footer-distributed">
			<div class="footer-left">
				<h3>Company<span>logo</span></h3>
				<p class="footer-links">
					<a href="#">Home</a>
					·
					<a href="#">Blog</a>
					·
					<a href="#">Pricing</a>
					·
					<a href="#">About</a>
					·
					<a href="#">Faq</a>
					·
					<a href="#">Contact</a>
				</p>
				<p class="footer-company-name">Company Name &copy; 2015</p>
			</div>
			<div class="footer-center">
				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>21 Revolution Street</span> Paris, France</p>
				</div>
				<div>
					<i class="fa fa-phone"></i>
					<p>+1 555 123456</p>
				</div>
				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">support@company.com</a></p>
				</div>
			</div>
			<div class="footer-right">
				<p class="footer-company-about">
					<span>About the company</span>
					Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
				</p>
				<div class="footer-icons">
					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>
				</div>
			</div>
		</footer>


<?php
session_start();
$_SESSION['quizNo']=$quizNo;
$_SESSION['ques']=$ques;
$_SESSION['ans']=$ans;
/*$_SESSION['stu']=$student;*/
?>


</body>
</html>