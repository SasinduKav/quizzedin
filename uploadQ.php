<html>
<head>
	<title>Quiz Upload</title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
	<script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
	
	<!--this is for the footer-->
	<link rel="stylesheet" href="css/footer.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
	
	<!--end footer-->

	<!-- Header Navigation -->
	<link rel="stylesheet" href="css/headNav.css">
	<style type="text/css">
		.btn {
		  -webkit-border-radius: 12;
		  -moz-border-radius: 12;
		  border-radius: 12px;
		  font-family: Georgia;
		  color: #000000;
		  font-size: 20px;
		  background: #b0b8bd;
		  padding: 8px 20px 9px 20px;
		  text-decoration: none;
		}

		.btn:hover {
		  background: #92c9eb;
		  background-image: -webkit-linear-gradient(top, #92c9eb, #3498db);
		  background-image: -moz-linear-gradient(top, #92c9eb, #3498db);
		  background-image: -ms-linear-gradient(top, #92c9eb, #3498db);
		  background-image: -o-linear-gradient(top, #92c9eb, #3498db);
		  background-image: linear-gradient(to bottom, #92c9eb, #3498db);
		  text-decoration: none;
		}
	</style>

</head>

<body>

<div class="mdl-layout__header" id="header" align="center">
	<h2>Welcome to QuizzedIn</h2>

</div>
	
  <div class="mdl-layout__header" style="background-color:#333;" id="header" align="center">
		<div style="padding-left:50px;padding-right:50px">
			<ul class="ul1">
			  <li class="li1"><a href="#home">Home</a></li>
			  <li class="li1"><a href="#news">News</a></li>
			  <li class="li1"><a href="#contact">Contact</a></li>
			  <li class="li1" style="float:right"><a href="#about">About</a></li>
			</ul>
		</div>
  </div>

  		<div>	

				<div style="padding-left:100px;background-color:white !important" >
					<form action="Login.php" method="post" autocomplete="off">
						<div class="mdl-textfield mdl-js-textfield" style="width:500px">
						   	<h3 style="font-family: Georgia;"><b>Quiz Uploading Form</b></h3>
					  	</div><br>
						<div class="mdl-textfield mdl-js-textfield">
					    	<input class="mdl-textfield__input" type="text" id="sample1" name="username">
					    	<label class="mdl-textfield__label" for="sample1">Quiz Name</label>
			  			</div>
			  			
			  			<br><br><br>
						<?php
						for($qno=1;$qno<21;$qno++){

						?>

						<div class="mdl-textfield mdl-js-textfield">
					    	<input class="mdl-textfield__input" type="text" id="sample1" name="username">
					    	<label class="mdl-textfield__label" for="sample1">Question <?php echo $qno ?> </label>
			  			</div>	
			  			<div class="mdl-textfield mdl-js-textfield" style="width:100px">
					    	<input class="mdl-textfield__input" type="text" id="sample1" name="username">
					    	<label class="mdl-textfield__label" for="sample1">Answer</label>
			  			</div>
			  			<div class="mdl-textfield mdl-js-textfield" style="width:200px">
				    	<input class="mdl-textfield__input" type="text" id="sample1" name="username">
					    	<label class="mdl-textfield__label" for="sample1">Answer 01</label>
			  			</div>
			  			<div class="mdl-textfield mdl-js-textfield" style="width:200px">
					    	<input class="mdl-textfield__input" type="text" id="sample1" name="username">
					    	<label class="mdl-textfield__label" for="sample1">Answer 02</label>	
			  			</div>	
			  			<div class="mdl-textfield mdl-js-textfield" style="width:200px">
				    	<input class="mdl-textfield__input" type="text" id="sample1" name="username">
					    	<label class="mdl-textfield__label" for="sample1">Answer 03</label>
			  			</div>
			  			<div class="mdl-textfield mdl-js-textfield" style="width:200px">
					    	<input class="mdl-textfield__input" type="text" id="sample1" name="username">
					    	<label class="mdl-textfield__label" for="sample1">Answer 04</label>	
			  			</div>

			  			<?php
			  			}

			  			?>
			  			<br>
			  			<div class="mdl-textfield mdl-js-textfield">
					    	<input type="submit" class="btn" name="Submit" value="SUBMIT">
			  			</div>
					</form>
				</div>
		</div>












<!--footer-->	
		<footer class="footer-distributed">
			<div class="footer-left">
				<h3>Company<span>logo</span></h3>
				<p class="footer-links">
					<a href="#">Home</a>
					·
					<a href="#">Blog</a>
					·
					<a href="#">Pricing</a>
					·
					<a href="#">About</a>
					·
					<a href="#">Faq</a>
					·
					<a href="#">Contact</a>
				</p>
				<p class="footer-company-name">Company Name &copy; 2015</p>
			</div>
			<div class="footer-center">
				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>21 Revolution Street</span> Paris, France</p>
				</div>
				<div>
					<i class="fa fa-phone"></i>
					<p>+1 555 123456</p>
				</div>
				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">support@company.com</a></p>
				</div>
			</div>
			<div class="footer-right">
				<p class="footer-company-about">
					<span>About the company</span>
					Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
				</p>
				<div class="footer-icons">
					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>
				</div>
			</div>
		</footer>

</body>
</html>