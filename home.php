<html>
<head>
	<title>Home</title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
	<script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>

	<style type="text/css">
	#header{
		padding-left: 5%;
	}
	.demo-card-wide.mdl-card {
  		width: 512px;
	}
	.demo-card-wide > .mdl-card__title {
  		color: #fff;
  		height: 176px;
  		background: url('Background_Card.png') center / cover;
	}
	.demo-card-wide > .mdl-card__menu {
  		color: #fff;
	}
	</style>
	
</head>
<body>
<div class="mdl-layout__header" id="header">
	<h2>Home</h2>

</div>
<div style="padding-top:1%;padding-left:2%;">
<div class="demo-card-wide mdl-card mdl-shadow--2dp">
  <div class="mdl-card__title">
    <h2 class="mdl-card__title-text">#loadquizname</h2>
  </div>
  <div class="mdl-card__supporting-text">
   #load short detail
  </div>
  <div class="mdl-card__actions mdl-card--border">
    <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
      Start!
    </a>
  </div>
  <div class="mdl-card__menu">
    <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
      <i class="material-icons">share</i>
    </button>
  </div>
</div>
</div>
</body>
</html>